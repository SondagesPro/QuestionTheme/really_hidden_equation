# Really hidden equation

Didn't display in HTML the equation part to realy hide the data to the user.

When you use equation question type for data to be hidden to user, you must put it in a dedicated group.

Else the equation are displayed on the HTML page then an user can see the data to be saved. This plugin offer the possibility to really hide the equation question, and then your data.

This is particuallary interesting for survey in one page or for equation at end.

## Survey without any style

![Proof](survey/questions/answer/equation/assets/proof.png?raw=true "Proof without any CSS of LimeSurvey issue.")

To get this with Firefox : Menu view, Page style, no style. For other browser : you can use [Web Developer plugin](https://chrispederick.com/work/web-developer/), or check with developer console.

## Installation and sage

1. Extract the download and upload the "really_hidden_equation" folder to ./upload/themes/question/.
2. Create an Equation question
3. Set the question setting "Question theme" to "really hidden equation".

The question text part still shown to user if you don't set hidden to true.

**You must use Equation advanced settings for your data, this plugin hide only the final equation, not the question text**

You can not use equation result in same page for display information to the user. Equation are evaluated only via server.

## Copyright and home page

- HomePage <https://extensions.sondages.pro>
- Copyright © 2020 Denis Chenu <https://sondages.pro>
- Licence : Expat/MIT <https://directory.fsf.org/wiki/License:Expat>
- [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/SondagesPro/) : [Donate on Liberapay](https://liberapay.com/SondagesPro/)
